import numpy as np
import ai2thor.controller
import random, time, pdb, json

controller = ai2thor.controller.Controller()
controller.local_executable_path = "/Users/khzeng/Desktop/exp/new_drone/ai2thor/unity/builds/thor-local-OSXIntel64.app/Contents/MacOS/thor-local-OSXIntel64"
### Note: here are two ports, the second port is for the new server aiming to receive the metadata from unity at every 0.02s
controller.start(player_screen_width=600, player_screen_height=600, start_unity=True, port=8200)

### Room 207 is buggy
scenes = ['FloorPlan{}_physics'.format(ele) for ele in range(201, 231)]

### 20 Objects
#object_types = ['AlarmClock', 'Apple', 'BasketBall', 'Glassbottle', 'Bowl', 'Bread', 'CellPhone', 'CreditCard',
#                'Cup', 'DishSponge', 'Egg', 'KeyChain', 'Newspaper', 'Pen', 'RemoteControl', 'SaltShaker',
#                'Spoon', 'ToiletPaper', 'Tomato', 'Watch']
object_types = json.load(open('object_type.json', 'r'))
object_types = list(object_types.keys())

def get_distanct(event_a, event_b):
    p_a = event_a.metadata['objects'][0]['position']
    p_b = event_b.metadata['objects'][0]['position']
    distance = (p_a['y'] - p_b['y'])**2 +\
               (p_a['x'] - p_b['x'])**2 +\
               (p_a['z'] - p_b['z'])**2
    distance = distance**0.5
    return distance

for _ in range(100):
    scene = random.choice(scenes)
    event = controller.reset(scene)
    event = controller.step(dict(action='Initialize', gridSize=0.25, continuous=True))
    event = controller.step(dict(action='FlyRandomStart', random_start=True))
    event = controller.step({'action' : 'ChangeFixedDeltaTime', 'fixedDeltaTime': 0.02})
    event = controller.step({'action' : 'ChangeTimeScale', 'timeScale': 0.25})
    event = controller.step({'action':'Look','horizon': -30})
    objectType = random.choice(object_types)
    objectRandom = True
    #meg = 80 + random.random()*80
    #angle_y = 1.2 + random.random()*0.6
    #angle_x = -0.5 + random.random()*1.0
    meg = 30
    angle_y = 1.8
    angle_x = -0
    event = controller.step(dict(action = 'LaunchDroneObject', moveMagnitude = meg, x = angle_x, y = angle_y, z = -1,
                                 objectName=objectType, objectRandom=objectRandom))
    events = [event]
    while len(events) < 30:
        event = controller.step(dict(action = 'FlyUp'))
        if get_distanct(event, events[-1]) < 0.00001:
            continue
        events.append(event)
    for i in range(10):
        print(events[i].metadata['objects'][0]['position'])
    pdb.set_trace()
    """
    ### Do random starting position
    agent_pos = event.metadata['agent']['position']
    event = controller.step({'action':'Look','horizon': -30})

    ### Prepare to launch
    objectType = random.choice(object_types)
    if objectType == 'RemoteControl':
        objectName = 'Remote'
    elif objectType == 'BasketBall':
        objectName = 'Ball'
    elif objectType == 'Glassbottle':
        objectName = 'Bottle'
    elif objectType == 'KeyChain':
        objectName = 'Keychain'
    else:
        objectName = objectType
    objectRandom = False

    ### Launch the object
    event = controller.step(dict(action = 'LaunchDroneObject', moveMagnitude = meg, x = angle_x, y = angle_y, z = 1,
                            objectName=objectType, objectRandom=objectRandom))

    stop, fail = False, False
    last_event_id = 0
    received_event = []
    while stop == False:
        events = list(controller.request_pool_queue.queue)

        ### check whether the second server starts to receive metadata
        if len(events) == last_event_id:
            time.sleep(0.01)
            continue

        ### check whether the object has been launched or not
        if 'Drone'+objectName not in events[-3].metadata['objects'][0]['name']:
            time.sleep(0.01)
            continue

        ### check the currentTime at first metadata we want to use is match to 0.00-0.01 second from launching
        if last_event_id == 0:
            if np.floor(events[-3].metadata['currentTime']*100) > 1:
                fail = True
                break
            else:
                ### collect the first two data
                received_event = [events[-3:-1]]
        ### check if the metadata is delayed to much
        elif len(events) - last_event_id > 5:
            fail = True
            break

        ### collect the data at time t
        received_event.append(events[-1])
        last_event_id = len(events)

        ### keep flybacking
        event = controller.step(dict(action = 'FlyTo',
                                     moveMagnitudeX = 0,
                                     moveMagnitudeY = 0,
                                     moveMagnitudeZ = 0.1,
                                     horizon = -30))

        ### check if the object is already at steady-state        
        if len(received_event) > 5:
            y = [np.abs(received_event[-1].metadata['objects'][0]['position']['y']-
                 received_event[-ei].metadata['objects'][0]['position']['y']) for ei in range(2,6)]
            if y[0] < 0.00001 and y[1] < 0.00001 and y[2] < 0.00001 and y[3] < 0.00001:
                stop = True
    if fail:
        print("this episode fails with sequence length "+str(len(received_event)))
        continue
    """
